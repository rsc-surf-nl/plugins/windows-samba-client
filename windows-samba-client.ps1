$Logfile = "C:\logs\windows-samba-client.log"

Function LogWrite
{
   Param ([string]$logstring)
   '{0:u}: {1}' -f (Get-Date), $logstring | Out-File $Logfile -Append
}

function testport($hostname, $port, $timeout) {
    $requestCallback = $state = $null
    $client = New-Object System.Net.Sockets.TcpClient
    $client.BeginConnect($hostname,$port,$requestCallback,$state)
    Start-Sleep -milli $timeOut
    if ($client.Connected) { $open = $true } else { $open = $false }
    $client.Close()
    [pscustomobject]@{hostname=$hostname;port=$port;open=$open}
}

function CheckIps($ips,$port) {
    ForEach ($ip in $ips) {
        $result = testport $ip 445 10
        if ($result.open -eq $true) { $ip }
    }
}

 Function Main { 
    LogWrite("Get variables")

    Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope LocalMachine -Force

    $user = [System.Environment]::GetEnvironmentVariable('SMB_USER')
    $password =[System.Environment]::GetEnvironmentVariable('SMB_PW')
    $sambashare = [System.Environment]::GetEnvironmentVariable('SMB_SHARE')

    LogWrite("Get local IP")
    $localIP = (Get-NetIPAddress -AddressFamily IPv4 | Where-Object { $_.InterfaceAlias -ne "Loopback Pseudo-Interface 1" }).IPAddress
    $ips = 1..255 | ForEach-Object {"10.10.10.$_"} | Where-Object { $_ -ne $localIP }

    $array = CheckIps $ips "445"

    if (!($array)) {
        LogWrite("No local IP SMB server found!")
    }

    foreach ($i in $array){ 
        $url = "\\" + $i +  "\" + $sambashare
        LogWrite("Url: $url")
        if(net use $url /user:$user $password){
          LogWrite("shortcut on startup for mounting")
          
          LogWrite("Creating folder and script")
          New-Item "C:\src-scripts\sambamount.cmd" -ItemType File -Value "@echo off" -Force
          Add-Content "C:\src-scripts\sambamount.cmd"  "`r`nnet use s: $url /user:$user $password`r`nexit"
          
          LogWrite("Creating shortcut in startup folder")
          $shell = New-Object -comObject WScript.Shell
          $shortcut = $shell.CreateShortcut("$env:ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp\sambamount.lnk")
          $shortcut.TargetPath = "cmd.exe"
          $shortcut.Arguments = "/c start /min C:\src-scripts\sambamount.cmd"
          $shortcut.WindowStyle = 7
          $shortcut.Save()
      } else {
          $netUseOutput = Invoke-Expression -Command "net use $url /user:$user $password 2>&1"
          LogWrite("unable to connect to $netUseOutput")
      }
    } 
}

try {
    Main
}
catch {
    LogWrite("$_")
    Throw $_   
}  
